#include <stdio.h>
#include <Windows.h>

#include "image_buffer.h"

#define NEW_IMAGE_SEMAPHORE_NAME L"Local\\new_image_semaphore"


image_buffer_writer::image_buffer_writer()
{
    buffer_index = 0;
}

int image_buffer_writer::init_left()
{
    return init(L"left");
}

int image_buffer_writer::init_right()
{
    return init(L"right");
}

//allocate shared memory for the images, image info and create a semaphore
int image_buffer_writer::init(wchar_t *side)
{
    wchar_t buffer_name[128];

    for (int i = 0; i < BUFFER_CHAIN_LENGTH; i++)
	{
        _snwprintf_s(buffer_name, sizeof(buffer_name), L"Local\\image_buffer_%ls_%d", side, i);
        if (create_shared_memory(buffer_name, &buffer_chain_handles[i], &buffer_chain[i], BUFFER_SIZE) < 0)
		{
            printf("Failed to create shared memory\n");
            return -1;
		}
	}

    _snwprintf_s(buffer_name, sizeof(buffer_name), L"Local\\image_info_%ls", side);
    if (create_shared_memory(buffer_name, &image_info_handle, &image_info_buffer, sizeof(image_info_t)) < 0)
	{
        printf("Failed to create shared memory\n");
        return -1;
	}

    _snwprintf_s(buffer_name, sizeof(buffer_name), L"%ls_%ls", NEW_IMAGE_SEMAPHORE_NAME, side);
    new_image_semaphore = CreateSemaphore(NULL, 0, 1, buffer_name);   
    if (new_image_semaphore == NULL)
	{
        printf("Failed to create semaphore\n");
        return -1;
	}
    
    return 0;
}

int image_buffer_writer::create_shared_memory(wchar_t *buffer_name, void **buffer_handle, unsigned char **buffer, size_t size)
{
    //allocate shared memory
    *buffer_handle = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, size, buffer_name);
    if (*buffer_handle == NULL)
    {
        printf("Could not create file mapping object (%d).\n",GetLastError());
        return -1;
    }
     
    //give write permission to buffer
    *buffer = (unsigned char *) MapViewOfFile(*buffer_handle, FILE_MAP_WRITE, 0, 0, size);

    if (*buffer == NULL)
    {
        printf("Could not map view of file (%d).\n", GetLastError());
        CloseHandle(*buffer_handle);
        return -1;
    }

    //zero out buffer - black image
    memset(*buffer, 0, size);

    return 0;
}

image_buffer_writer::~image_buffer_writer()
{
    //free shared memory
    for (int i = 0; i < BUFFER_CHAIN_LENGTH; i++)
	{
        UnmapViewOfFile(buffer_chain[i]);
        CloseHandle(buffer_chain_handles[i]);
	}

    UnmapViewOfFile(image_info_buffer);
    CloseHandle(image_info_handle);

    CloseHandle(new_image_semaphore);
}

//return pointers to the next left and right image buffer
//this allows functions to write directly to the buffer without copying
unsigned char *image_buffer_writer::next_image_buffer(image_info_t *image_header)
{
    return buffer_chain[(buffer_index + 1) % BUFFER_CHAIN_LENGTH];
}

//assumes the images have been updated manually and updates the buffer index
void image_buffer_writer::write_image(image_info_t *image_header)
{
    buffer_index = (buffer_index + 1) % BUFFER_CHAIN_LENGTH;
    image_header->buffer_index = buffer_index;
    //XXX should probably not repeatedly memcpy, just edit the poitner
    memcpy(image_info_buffer, image_header, sizeof(image_info_t));

    //increase semaphore count by one
    ReleaseSemaphore(new_image_semaphore, 1, NULL);
}

//copies the images provided into shared memory and updates the buffer index
void image_buffer_writer::write_image(image_info_t *image_header, unsigned char *image)
{
    buffer_index = (buffer_index + 1) % BUFFER_CHAIN_LENGTH;
    image_header->buffer_index = buffer_index;
    memcpy(buffer_chain[buffer_index], image, image_header->size);
    memcpy(image_info_buffer, image_header, sizeof(image_info_t));

    //increase semaphore count by one
    ReleaseSemaphore(new_image_semaphore, 1, NULL);
}

//get a pointer to the shared memory buffer at particular index
unsigned char *image_buffer_writer::get_buffer(int index)
{
    return buffer_chain[index % BUFFER_CHAIN_LENGTH];
}

image_buffer_reader::image_buffer_reader()
{
    image_offset = 0;
}

int image_buffer_reader::init_left()
{
    return init(L"left");
}

int image_buffer_reader::init_right()
{
    return init(L"right");
}

//open all the shared memory for images, image info and open the semaphore
int image_buffer_reader::init(wchar_t *side)
{
    wchar_t buffer_name[128];

    for (int i = 0; i < BUFFER_CHAIN_LENGTH; i++)
	{
        _snwprintf_s(buffer_name, sizeof(buffer_name), L"Local\\image_buffer_%ls_%d", side, i);
        if (open_shared_memory(buffer_name, &buffer_chain_handles[i], &buffer_chain[i], BUFFER_SIZE) < 0)
		{
            printf("Failed to open shared memory\n", GetLastError());
            return -1;
		}
	}

    _snwprintf_s(buffer_name, sizeof(buffer_name), L"Local\\image_info_%ls", side);
    if (open_shared_memory(buffer_name, &image_info_handle, &image_info_buffer, sizeof(image_info_t)) < 0)
	{
        printf("Failed to open shared memory\n");
        return -1;
	}

    _snwprintf_s(buffer_name, sizeof(buffer_name), L"%ls_%ls", NEW_IMAGE_SEMAPHORE_NAME, side);
    new_image_semaphore = OpenSemaphore(SYNCHRONIZE | SEMAPHORE_MODIFY_STATE, TRUE, buffer_name);
    if (new_image_semaphore == NULL)
	{
        printf("Failed to open semaphore (%d)\n", GetLastError());
        return -1;
	}
    
    return 0;
}

int image_buffer_reader::open_shared_memory(wchar_t *buffer_name, void **buffer_handle, unsigned char **buffer, size_t size)
{
    //open the shared memory
    *buffer_handle = OpenFileMapping(FILE_MAP_READ, FALSE, buffer_name);

   if (*buffer_handle == NULL)
   {
      printf("Could not open file mapping object (%d).\n", GetLastError());
      return -1;
   }

   *buffer = (unsigned char *) MapViewOfFile(*buffer_handle, FILE_MAP_READ, 0, 0, size);

   if (*buffer == NULL)
   {
      printf("Could not map view of file (%d).\n", GetLastError());
      CloseHandle(*buffer_handle);
      return -1;
   }

   return 0;
}

image_buffer_reader::~image_buffer_reader()
{
    //close the shared memory
    for (int i = 0; i < BUFFER_CHAIN_LENGTH; i++)
	{
        UnmapViewOfFile(buffer_chain[i]);
        CloseHandle(buffer_chain_handles[i]);
	}

    UnmapViewOfFile(image_info_buffer);
    CloseHandle(image_info_handle);

    CloseHandle(new_image_semaphore);
}

void image_buffer_reader::get_image_buffer(image_info_t **image_info, unsigned char **image)
{
    //wait for a new image to be ready
    //block on the semaphore and decreases semaphore count by 1
    WaitForSingleObject(new_image_semaphore, INFINITE);
    *image_info = (image_info_t *)image_info_buffer;
    *image = buffer_chain[(*image_info)->buffer_index + image_offset];
}

//XXX unused, but was intended if frames were slightly out of sync
void image_buffer_reader::set_image_offset(int offset)
{
    image_offset = offset % BUFFER_CHAIN_LENGTH;
}
