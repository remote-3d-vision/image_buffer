#ifndef __IMAGE_BUFFER_H__
#define __IMAGE_BUFFER_H__

#define BUFFER_CHAIN_LENGTH 5

//buffer size is arbitrarily large 1920*1080*4
#define BUFFER_SIZE 8294400

//XXX not used
#define FORMAT_R8G8B8 1
#define FORMAT_R8G8B8A8 2

#define _CRT_NON_CONFORMING_SWPRINTFS

typedef struct image_info_s
{
    int width;
    int height;
    int format;
    size_t size;
    int buffer_index;
} image_info_t;

class image_buffer
{
    protected:
        //---------shared memory------------
        //a semaphore will allow image_buffer_reader to read the image
        //info which will contain the next buffer in the buffer chain to read from
        void *new_image_semaphore;

        //contains width, height, format, size, buffer_index
        void *image_info_handle;
        unsigned char *image_info_buffer;

        //chain of buffers that will hold the last X images written
        void *buffer_chain_handles[BUFFER_CHAIN_LENGTH];
        unsigned char *buffer_chain[BUFFER_CHAIN_LENGTH];
        //---------shared memory------------
};

class image_buffer_writer : public image_buffer
{
    private:
		int buffer_index;
        int create_shared_memory(wchar_t *buffer_name, void **buffer_handle, unsigned char **buffer, size_t size);
        int init(wchar_t *side);

    public:
        image_buffer_writer();
        ~image_buffer_writer();
        int init_left();
        int init_right();
        unsigned char *next_image_buffer(image_info_t *image_info);
        void write_image(image_info_t *image_info);
        void write_image(image_info_t *image_info, unsigned char *image);
        unsigned char *get_buffer(int index);
};

class image_buffer_reader : public image_buffer
{
    private:
		int image_offset;
        int open_shared_memory(wchar_t *buffer_name, void **buffer_handle, unsigned char **buffer, size_t size);
        int init(wchar_t *side);

    public:
        image_buffer_reader();
        ~image_buffer_reader();
        int init_left();
        int init_right();
        void get_image_buffer(image_info_t **image_info, unsigned char **image);
        void set_image_offset(int offset);
};

#endif __IMAGE_BUFFER_H__
